# RNA or Protein Binding Molecule Classifier

**Author**: Jozef Fulop  
**Institution**: UCT Prague, FIT CTU in Prague, Czech Republic  
**Date**: December 2023

---

## Project Overview
This project is centered on building a binary classification model that distinguishes between RNA-binding and protein-binding small molecules. The model harnesses the power of machine learning, specifically graph neural networks (GNNs), to analyze and predict the binding characteristics of small molecules. The distinction between RNA-binding and protein-binding molecules is vital for various applications in biochemical research, drug discovery, and therapeutic development.

## Results

The Graph Convolutional Network (GCN) model displayed the most stable loss minimization and highest accuracy. However, the Message Passing Neural Network (MPNN) and Graph Attention Network (GAT) models showcased specific strengths, with potential in applications requiring precise molecular structure recognition and classification.

The model evaluation yielded the following comparative performance metrics:

| Model   | Accuracy | Precision | Recall | F1 Score | AUC   |
|---------|----------|-----------|--------|----------|-------|
| GCN     | 78.96%   | 81.11%    | 75.09% | 77.98%   | 87.39%|
| MPNN    | 67.11%   | 65.15%    | 72.45% | 68.61%   | 72.76%|
| GAT     | 73.87%   | 75.32%    | 70.39% | 72.77%   | 81.63%|


The following visualizations provide additional insights into the model's performance:

#### Comparative Performance Metrics
<img src="visuals/comprative_performance_metrics.png" alt="Comparative Performance Metrics" width="700"/>

*Bar chart comparing the performance metrics of the GCN, MPNN, and GAT models.*

#### Training and Validation Loss Curves
<img src="visuals/loss_paper.png" alt="Loss Curves" width="700"/>

*Loss curves for the GCN, MPNN, and GAT models, showcasing the training and validation loss over epochs with points of early stopping.*


These visualizations can be found in the `visuals/` directory and offer a visual understanding of the models' performance.



## Data
The dataset, assembled from various vendors' libraries, contains molecular structures in graph form. The comprehensive dataset includes 42,091 RNA-binding and 460,160 protein-binding molecules, offering a diverse set for the classification model.

## Methods
We employed PyTorch and the Deep Graph Library (DGL) for model implementation. The DGL-LifeSci library was instrumental in converting molecular data into graph representations suitable for GNN processing.

### Graph Convolutional Network (GCN)
The GCN model used a two-layer architecture with graph convolutional layers followed by a fully connected layer for binary classification. This design effectively captured the complex interactions within the molecular graphs.

### Message Passing Neural Network (MPNN)
The MPNN model incorporated an edge convolution (EdgeConv) operation and a custom message-passing mechanism. The messages were a combination of source and destination node features, globally pooled to represent the entire graph.

### Graph Attention Network (GAT)
The GAT model implemented an attention mechanism through GATConv layers, allowing variable importance to different nodes. This attention-based approach was aimed at improving classification precision by focusing on more influential nodes within the graph.

Graphs were created from molecular dataframes with each molecule represented as a bi-directed DGLGraph. Canonical featurizers generated node and edge features from RDKit molecular objects.


## Visualizations
Several visualizations were generated to interpret the model's performance:
- Training and Validation Loss Curves: Demonstrating the training process and points of early stopping.
- ROC Curves: Illustrating the model's discriminative capability with AUC values for GCN, MPNN, and GAT.
- Confidence Histograms: Depicting the model's confidence distribution in its predictions.

Key visualizations are included to interpret the model's performance:
- `comprative_performance_metrics.png`: A bar chart comparing the performance metrics of the GCN, MPNN, and GAT models.
- `loss_paper.png`: The loss curves for the models, showcasing training and validation loss over epochs.

These visualizations can be found in the `visuals/` directory and provide insights into model training dynamics and comparative efficacy.

## Top Molecules
The top 20 molecules with the highest probabilities for the positive class, visualized and sorted, are available in the following directories:
- `top_molecules_gcn/`: Top 20 molecules identified by the GCN model.
- `top_molecules_mpnn/`: Top 20 molecules identified by the MPNN model.
- `top_molecules_gat/`: Top 20 molecules identified by the GAT model.

Each directory contains visual representations of the molecules, allowing for a quick review of the compounds that the models predict with the highest confidence as RNA-binding molecules.


## Usage
To replicate the experiments or apply the model to new datasets, follow these steps:

1. **Environment Setup:**
   - Clone the project repository to your local machine using the command:
     ```shell
     git clone https://gitlab.fit.cvut.cz/fulopjoz/mvi-sp
     ```
   - Navigate to the repository directory and run the `create_gnn_env.sh` script to create a virtual environment specifically for GNNs:
     ```shell
     cd path_to_repository
     ./create_gnn_env.sh
     ```
   - Activate the environment using conda:
     ```shell
     conda activate gnn_env
     ```

2. **Model Training:**
   - Execute the provided Jupyter Notebook within the activated environment. It will guide you through data preprocessing, model training, and validation steps.

3. **Evaluation:**
   - Use the visualizations and metrics output by the notebook to assess the model performance. The visualizations include ROC curves, loss curves, and histograms of model confidence.

4. **Visualize Top Molecules:**
   - Explore the top molecules predicted by each model, available in their respective directories `top_molecules_gcn/`, `top_molecules_mpnn/`, and `top_molecules_gat/`.

By following these steps, you can replicate the model's performance as documented in this project or use the models as a baseline for further research and development.


## Dependencies
- Python 3.7
- PyTorch
- DGL
- DGL-LifeSci
- Scikit-learn
- Matplotlib
- Seaborn

## Repository Structure
- `data/`: The dataset of molecular structures.
- `models/`: Saved models and checkpoints.
- `visuals/`: Visualization outputs such as loss curves, ROC curves, and confidence histograms.
- `scripts/`: Utility scripts for environment setup and data preprocessing.
- `README.md`: Documentation and comprehensive guide for the project.
- `top_molecules_gcn/`, `top_molecules_mpnn/`, and `top_molecules_gat/`: Top 20 predicted RNA-binding molecules

